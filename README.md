# Axur Code Challenge Project

A ReactJs project with Bootstrap 4.

## Installation

To run this project, you need to install:

- [NodeJS](https://nodejs.org/en/)
- [Git](https://git-scm.com/)

### Setup

After installing all the dependencies above, please clone de Git repository.

User this command:

```bash
git clone https://bitbucket.org/llinhares1/axurtest.git
```

After that, go to the folder and open the command line and type:

```bash
npm install
npm start
```

Finally, the project will be open on your browser.

To build this run the command:

```bash
npm build
npm install -g serve
serve -s build
```