import React, {Component} from 'react';

class Button extends Component{
  constructor(props){
    super(props)
    this.state = {
      id: this.props.id,
      type: this.props.type,
      color: this.props.color,
      value: this.props.value
    }
  }

  render(){
    return(
      <div className="form-group">
        <button id={this.state.id} type={this.state.type} className={"btn " + this.state.color}>{this.state.value}</button>
      </div>
    );
  }
}

export default Button;