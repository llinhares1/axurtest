import React, {Component} from 'react';
import './contactsection.scss';
import Input from './input';
import Button from './button';

class ContactSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
    };
  }
  
  render() {
    return (
      <div className="container pt-5 pb-5">
        <div className="row">
          <div className="col-sm-12 text-center pb-5">
            <h3>-- Get In Touch --</h3>
          </div>
        </div>
        <form>
          <div className="row">
            <div className="col-sm-12 col-md-4">
              <Input id="email" type="text" name="name" placeholder="Name" label="Name" required={true} />
            </div>
            <div className="col-sm-12 col-md-4">
              <Input id="email" type="email" name="email" placeholder="email@email.com" label="E-mail" required={true} />
            </div>
            <div className="col-sm-12 col-md-4">
              <Input id="email" type="text" name="subject" placeholder="Subject" label="Subject" required={true} />
            </div>
            <div className="col-sm-12">
              <div className="form-group">
                <label htmlFor="message">Message</label>
                <textarea className="form-control" id="message" rows="3"></textarea>
              </div>
            </div>
          </div>
          <Button id="login" type="submit" color="btn-primary" value="Send" />
        </form>
      </div>
    );
  }
}

export default ContactSection;