import React, {Component} from 'react';
import './footer.scss';
import $ from 'jquery';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faFacebookF, faPinterestP, faTwitter, faBitbucket} from '@fortawesome/free-brands-svg-icons';

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: [
        {id: 1, socialIcon: faFacebookF, title: "Facebook", link: "https://facebook.com"},
        {id: 2, socialIcon: faPinterestP, title: "Pinterest", link: "https://pinterest.com/"},
        {id: 3, socialIcon: faTwitter, title: "Twitter", link: "https://twitter.com/"},
        {id: 4, socialIcon: faBitbucket, title: "Bitbucket", link: "https://bitbucket.org"},
      ],
    };
  }

  componentDidMount() {
    $('[data-toggle="tooltip"]').tooltip();
  }
  
  componentDidUpdate() {
    $('[data-toggle="tooltip"]').tooltip();
  }
  
  render() {
    return (
      <div className="footer container-fluid pt-5 pb-5">
        <footer className="text-center">
          {this.state.value.map((item)=>{
            return(
              <a key={item.id} href={item.link} target="_blank" rel="noopener noreferrer">
                <div className="social" data-toggle="tooltip" data-placement="top" title={item.title}>
                  <FontAwesomeIcon icon={item.socialIcon} />
                </div>
              </a>
            );
          })}
        </footer>
      </div>
    );
  }
}

export default Footer;