import React, {Component} from 'react';
import './navbar.scss';
import Logo from '../assets/images/logo.svg';

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
    };
  }
  
  render() {
  return (
    <nav className="navbar">
      <a className="navbar-brand m-auto" href="/">
      <img src={Logo} width="30" height="30" alt="" />
      </a>
    </nav>
  );
  }
}

export default NavBar;