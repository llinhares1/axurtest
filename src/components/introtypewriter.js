import React, {Component} from 'react';
import './introtypewriter.scss';
import Typewriter from 'typewriter-effect';
import {Link} from 'react-scroll';

class IntroTypeWriter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
    };
  }

  bubble = () => {
    let span = []
    for(let i = 0; i <= 40; i++) {
      span.push(<span key={i} className="bubble-bg"></span>);
    }
    return span;
  }
  
  render() {
    return (
      <div className="container-fluid text-center intro">
        <Typewriter onInit={(typewriter) => {
            typewriter.typeString("Hello!")
              .pauseFor(1500)
              .deleteAll()
              .typeString("I'm Lucas Linhares")
              .pauseFor(1500)
              .deleteAll()
              .typeString("Front-End Developer")
              .pauseFor(1500)
              .deleteAll()
              .typeString("Scroll down to see a little more about me.")
              .start();
          }}
        />
        {this.bubble()}
        <Link
          activeClass="active"
          to="about-me"
          spy={true}
          smooth={true}
          offset={0}
          duration={500}
        >
          <div className="see-more">
            See More <div></div>
          </div>
        </Link>
      </div>
    );
  }
}

export default IntroTypeWriter;