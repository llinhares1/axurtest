import React, {Component} from 'react';

class Input extends Component{
  constructor(props){
    super(props)
    this.state = {
      id: this.props.id,
      name: this.props.name,
      type: this.props.type,
      placeholder: this.props.placeholder,
      label: this.props.label,
      required: this.props.required,
      value: ""
    }
  }

  render(){
    return(
      <div className="form-group">
        <label htmlFor="exampleInputEmail1">{this.state.label}</label>
        <input type={this.state.type} className="form-control" id={this.state.id} placeholder={this.state.placeholder} required={this.state.required} />
      </div>
    );
  }
}

export default Input;