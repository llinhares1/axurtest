import React, {Component} from 'react';
import './aboutsection.scss';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class AboutSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.name,
      description: this.props.description,
      profileImg: this.props.profileImg,
      value: this.props.cardList
    };
  }
  
  render() {
    return (
      <div id="about-me" className="pt-5 pb-5">
        <div className="container">
          <div className="row">
            <div className="col-sm-12 text-center pb-5">
              <h3>-- About Me --</h3>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12 col-md-2">
              <div className="photo-box m-auto">
                <img src={this.state.profileImg} className="img-fluid" alt="" />
              </div>
            </div>
            <div className="col-sm-12 col-md-10 description">
              <h1>{this.state.name}</h1>
              <p>{this.state.description}</p>
            </div>
          </div>
          <div className="row">
            {this.state.value.map((item)=>{
              return(
                <div key={item.id} className="col-sm-12 col-md-4 my-auto pt-3">
                  <div className="card">
                    <img src={item.cardImg} className="card-img-top gray-filter" alt="" />
                    <div className="card-body">
                      <h5 className="card-title">{item.cardTitle}</h5>
                      <p className="card-text">{item.cardText}</p>
                    </div>
                    <ul className="list-group list-group-flush">
                      {item.cardList.map((list)=>{
                        return(
                          <li key={list.idList} className="list-group-item">
                            <FontAwesomeIcon icon={list.ListIcon} /> {list.listTitle}
                            <div className="progress">
                              <div className={"progress-bar " + list.listClass} role="progressbar" aria-valuenow={list.listPercentage} aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export default AboutSection;