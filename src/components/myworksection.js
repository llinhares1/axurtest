import React, {Component} from 'react';
import './myworksection.scss';

class MyWorkSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value,
    };
  }
  
  render() {
    return (
      <div className="container-fluid my-works pt-5 pb-5">
        <div className="row">
          <div className="col-sm-12 text-center pb-5">
            <h3>-- My Works --</h3>
          </div>
        </div>
        <div className="row">
          {this.state.value.map((item)=>{
            return(
              <div key={item.id} className="col-sm-12 col-md-3 pt-3">
                <div className="card bg-dark text-white">
                  <img src={item.img} className="card-img" alt="..." />
                  <div className="card-img-overlay">
                    <div className="info-container">
                      <h5 className="card-title">{item.title}</h5>
                      <a href={item.link} className="" target="_blank" rel="noopener noreferrer">
                        <div className="see-more">
                          See More <div></div>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default MyWorkSection;