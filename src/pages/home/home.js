import React, {Component} from 'react';
import './home.scss';
import NavBar from '../../components/navbar';
import IntroTypeWriter from '../../components/introtypewriter';
import AboutSection from '../../components/aboutsection';
import MyWorkSection from '../../components/myworksection';
import ContactSection from '../../components/contactsection';
import Footer from '../../components/footer';

/*About*/
import Me from '../../assets/images/me.png';
import Music from '../../assets/images/music.png';
import Development from '../../assets/images/development.png';
import Creative from '../../assets/images/creative.png';

import {faDrum, faGuitar} from '@fortawesome/free-solid-svg-icons';
import {faHtml5, faCss3Alt, faJs, faAdobe} from '@fortawesome/free-brands-svg-icons';

/*My Works*/
import Project1 from '../../assets/images/project1.jpg';
import Project2 from '../../assets/images/project2.jpg';
import Project3 from '../../assets/images/project3.jpg';
import Project4 from '../../assets/images/project4.jpg';
import Project5 from '../../assets/images/project5.jpg';
import Project6 from '../../assets/images/project6.jpg';

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
    };
  }

  bubble = () => {
    let span = []
    for(let i = 0; i <= 40; i++) {
      span.push(<span key={i} className="bubble-bg"></span>);
    }
    return span;
  }
  
  render() {
    return (
      <div className="home">
        <div className="header">
          <NavBar />
          <IntroTypeWriter />
        </div>
        <AboutSection
          name="Lucas Linhares"
          description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum lectus non mi facilisis dignissim. Vestibulum quis risus non est facilisis aliquam eu vitae arcu. Donec vulputate metus ut lacinia faucibus. Proin facilisis arcu ac mi imperdiet facilisis. Donec mattis justo magna, vel elementum magna pretium sit amet. Praesent at auctor leo. Mauris nec mi rhoncus, tincidunt velit fringilla, condimentum ante."
          profileImg={Me}
          cardList={[
              {
                id: 1,
                cardImg: Music,
                cardTitle: "Music",
                cardText: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum lectus non mi facilisis dignissim.",
                cardList: [
                  {idList: 1, ListIcon: faDrum, listTitle: "Drum", listClass: "drum", listPercentage: 80},
                  {idList: 2, ListIcon: faGuitar, listTitle:" Guitar", listClass: "guitar", listPercentage: 50}
                ],
              },
              {
                id: 2,
                cardImg: Development,
                cardTitle: "Development",
                cardText: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum lectus non mi facilisis dignissim.",
                cardList: [
                  {idList: 1, ListIcon: faHtml5, listTitle: "HTML5", listClass: "html5", listPercentage: 95},
                  {idList: 2, ListIcon: faCss3Alt, listTitle:" CSS3", listClass: "css3", listPercentage: 95},
                  {idList: 3, ListIcon: faJs, listTitle:" JavaScript", listClass: "javascript", listPercentage: 85}
                ],
              },
              {
                id: 3,
                cardImg: Creative,
                cardTitle: "Creative",
                cardText: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum lectus non mi facilisis dignissim.",
                cardList: [
                  {idList: 1, ListIcon: faAdobe, listTitle: "Adobe Photoshop", listClass: "photoshop", listPercentage: 80},
                  {idList: 2, ListIcon: faAdobe, listTitle:" Adobe Illustrator", listClass: "illustrator", listPercentage: 50}
                ]
              }
            ]}
        />
        <MyWorkSection
          value={[
            {id: 1, img: Project1, title: "Project 1", link: "https://www.google.com"},
            {id: 2, img: Project2, title: "Project 2", link: "https://www.google.com"},
            {id: 3, img: Project3, title: "Project 3", link: "https://www.google.com"},
            {id: 4, img: Project4, title: "Project 4", link: "https://www.google.com"},
            {id: 5, img: Project5, title: "Project 5", link: "https://www.google.com"},
            {id: 6, img: Project6, title: "Project 6", link: "https://www.google.com"},
          ]}
        />
        <ContactSection />
        <Footer />
      </div>
    );
  }
}

export default HomePage;